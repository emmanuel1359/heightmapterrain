#include "DemoApp.h"

#include "OgreVolumeTextureSource.h"
#include "OgreVolumeCSGSource.h"
#include "OgreRay.h"
#include "myMeshBuilderCallback.h"

#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Shapes/OgreBulletCollisionsCapsuleShape.h"


using namespace Ogre;
using namespace OgreBites;


DemoApp::DemoApp()
{
	mTerrainGroup = 0;
	mTerrainGlobals = 0;
	mVolumeRoot = 0;
	mVolumeRootNode = 0;
	mMouseState = 0;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

DemoApp::~DemoApp()
{
m_pLog->logMessage("-> ~DemoApp");

	if (mTerrainGroup)
	{
		OGRE_DELETE mTerrainGroup;
		OGRE_DELETE mTerrainGlobals;
	}
m_pLog->logMessage("-> ~DemoApp2");

	if (mVolumeRoot)
	{
		delete mVolumeRoot->getChunkParameters()->src;
		OGRE_DELETE mVolumeRoot;
	}

	if (m_BulletWorld)
	{
		// OgreBullet physic delete - RigidBodies
		std::deque<OgreBulletDynamics::RigidBody *>::iterator itBody = mBodies.begin();
		while (mBodies.end() != itBody)
		{
			OGRE_DELETE *itBody;
			++itBody;
		}
m_pLog->logMessage("-> ~DemoApp4");
		// OgreBullet physic delete - Shapes
		std::deque<OgreBulletCollisions::CollisionShape *>::iterator itShape = mShapes.begin();
		while (mShapes.end() != itShape)
		{
			OGRE_DELETE *itShape;
			++itShape;
		}
m_pLog->logMessage("-> ~DemoApp5");
		mBodies.clear();
		mShapes.clear();
		OGRE_DELETE m_BulletWorld->getDebugDrawer();
m_pLog->logMessage("-> ~DemoApp6");
		m_BulletWorld->setDebugDrawer(0);
		OGRE_DELETE m_BulletWorld;
	}
m_pLog->logMessage("<- ~DemoApp");
}




//|||||||||||||||||||||||||||||||||||||||||||||||

void DemoApp::startDemo()
{
	new OgreFramework();
	if(!OgreFramework::getSingletonPtr()->initOgre("Heigthmap Terrain v1.0", this, this))
		return;

	m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pSceneMgr;
	m_pLog = OgreFramework::getSingletonPtr()->m_pLog;

	m_bShutdown = false;

	m_pLog->logMessage("Demo initialized!");

	setupDemoScene();
	runDemo();
}





//|||||||||||||||||||||||||||||||||||||||||||||||

void DemoApp::setupDemoScene()
{
	// label tray
	mInfoLabel = OgreFramework::getSingletonPtr()->m_pTrayMgr->createLabel(OgreBites::TL_TOP, "TInfo", "", 350);


//	m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
	m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
//	m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	m_pSceneMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);
	m_pSceneMgr->setAmbientLight(Ogre::ColourValue::White);


	// le soleil
	mLightPosition = Ogre::Vector3(00, 400, 00);
	mLightRotationAxe = Ogre::Vector3::UNIT_X;
	mLightState = true;

	mLightPivot = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	mLightPivot->rotate(mLightPosition, Ogre::Angle(111));
	// Create a light, use default parameters
	mLight = m_pSceneMgr->createLight("Light");
	mLight->setPosition(mLightPosition);
	mLight->setDiffuseColour(Ogre::ColourValue(0.6, 0.6, 0.6));
	mLight->setSpecularColour(Ogre::ColourValue(0.5, 0.5, 0.5));
	mLight->setVisible(mLightState);
	//mLights->setAttenuation(400, 0.1 , 1 , 0);
	// Attach light
	mLightPivot->attachObject(mLight);
	// Create billboard for light
	mLightFlareSet = m_pSceneMgr->createBillboardSet("Flare");
	mLightFlareSet->setMaterialName("LightFlare");
	mLightPivot->attachObject(mLightFlareSet);
	mLightFlare = mLightFlareSet->createBillboard(mLightPosition);
	mLightFlare->setColour(mLight->getDiffuseColour());
	mLightFlareSet->setVisible(mLightState);


	createTerrain(mLight);
	initBullet();
//	initVolumeTerrain();


//	m_pSceneMgr->setFog(Ogre::FOG_LINEAR, Ogre::ColourValue(0.9, 0.9, 0.9), 0.0, 50, 500);
//	m_pSceneMgr->setFog(Ogre::FOG_EXP, Ogre::ColourValue(0.9, 0.9, 0.9), 0.005);
//	m_pSceneMgr->setFog(Ogre::FOG_EXP2, Ogre::ColourValue(0.9, 0.9, 0.9), 0.003);
}



//|||||||||||||||||||||||||||||||||||||||||||||||

void DemoApp::runDemo()
{
	m_pLog->logMessage("Start main loop...");

	double startTime = 0;
	m_timeSinceLastFrame = 0;

	OgreFramework::getSingletonPtr()->m_pRenderWnd->resetStatistics();

	while(!m_bShutdown && !OgreFramework::getSingletonPtr()->isOgreToBeShutDown())
	{
		if(OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;

		Ogre::WindowEventUtilities::messagePump();

		if(OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
		{
			startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();

			OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
			OgreFramework::getSingletonPtr()->m_pMouse->capture();

			OgreFramework::getSingletonPtr()->updateOgre(m_timeSinceLastFrame);

			frameRenderingQueued(OgreFramework::getSingletonPtr()->getFrameEvent());



			OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();

			m_timeSinceLastFrame = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;
		}
		else
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			Sleep(1000);
#else
			sleep(1);
#endif
		}
	}

	m_pLog->logMessage("Main loop quit");
	m_pLog->logMessage("Shutdown OGRE...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

	static Ogre::Real mToggle = 0.5;    // The time left until next toggle

	mToggle -= m_timeSinceLastFrame;


	if(m_BulletWorld)
	{
		// create and throw a box if 'B' is pressed
		if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_B) && mToggle <=0)
		{
			Vector3 size = Vector3::ZERO;   // size of the box
			// starting position of the box
			Vector3 position = (OgreFramework::getSingletonPtr()->m_pCamera->getDerivedPosition() +
							OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10);
			// create an ordinary, Ogre mesh with texture
			Entity *entity = m_pSceneMgr->createEntity(
			   "Box" + StringConverter::toString(mNumEntitiesInstanced),
			   "cube.mesh");
			entity->setCastShadows(true);
			// we need the bounding box of the box to be able to set the size of the Bullet-box
			AxisAlignedBox boundingB = entity->getBoundingBox();
			size = boundingB.getSize(); size /= 2.0f; // only the half needed
			size *= 0.96f;   // Bullet margin is a bit bigger so we need a smaller size
						   // (Bullet 2.76 Physics SDK Manual page 18)
			entity->setMaterialName("Examples/BumpyMetal");
			SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
			node->attachObject(entity);
			Ogre::Real scale = 0.01f;


			size.x *= scale;
			size.y *= 2*scale;
			size.z *= 3*scale;

			node->scale(scale, 2*scale, 3*scale);   // the cube is too big for us
//			size *= scale;                  // don't forget to scale down the Bullet-box too
			// after that create the Bullet shape with the calculated size


			OgreBulletCollisions::BoxCollisionShape *sceneBoxShape = new OgreBulletCollisions::BoxCollisionShape(size);
			// and the Bullet rigid body
			OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
			   "defaultBoxRigid" + StringConverter::toString(mNumEntitiesInstanced),
			   m_BulletWorld);
			defaultBody->setShape(   node,
						   sceneBoxShape,
						   0.2f,         // dynamic body restitution
						   0.7f,         // dynamic body friction
						   10.f,          // dynamic bodymass
						   position,      // starting position of the box
						   Quaternion(0,0,0,1));// orientation of the box
			mNumEntitiesInstanced++;
			defaultBody->setLinearVelocity(
				  OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10.0f ); // shooting speed
			   // push the created objects to the deques
			mShapes.push_back(sceneBoxShape);
			mBodies.push_back(defaultBody);
			mToggle = 0.5;
		}

		// create and throw a cylinder if 'C' is pressed
		if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_C) && mToggle <=0)
		{
			Vector3 size = Vector3::ZERO;   // size of the box
			// starting position of the box
			Vector3 position = (OgreFramework::getSingletonPtr()->m_pCamera->getDerivedPosition() +
							OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10);
			// create an ordinary, Ogre mesh with texture
			Entity *entity = m_pSceneMgr->createEntity(
			   "column" + StringConverter::toString(mNumEntitiesInstanced),
			   "column.mesh");
			entity->setCastShadows(true);
			// we need the bounding box of the box to be able to set the size of the Bullet-box
			AxisAlignedBox boundingB = entity->getBoundingBox();
			size = boundingB.getSize(); size /= 2.0f; // only the half needed
//			size *= 0.96f;   // Bullet margin is a bit bigger so we need a smaller size
						   // (Bullet 2.76 Physics SDK Manual page 18)
			entity->setMaterialName("Examples/BumpyMetal");
			SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
			node->attachObject(entity);
			Ogre::Real scale = 0.04f;
			node->scale(scale, scale, scale);   // the cube is too big for us
			size *= scale;                  // don't forget to scale down the Bullet-box too
			// after that create the Bullet shape with the calculated size
			OgreBulletCollisions::CylinderCollisionShape *sceneCylinderShape = new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3(scale*30, scale*300, scale*30), Ogre::Vector3::UNIT_Y);
			// and the Bullet rigid body
			OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
			   "defaultBoxRigid" + StringConverter::toString(mNumEntitiesInstanced),
			   m_BulletWorld);


			defaultBody->setShape(   node,
						   sceneCylinderShape,
						   0.5f,         // dynamic body restitution
						   0.2f,         // dynamic body friction
						   100.f,          // dynamic bodymass
						   position,      // starting position of the box
						   Quaternion(0, 0, 0, 1));// orientation of the box
			mNumEntitiesInstanced++;
			defaultBody->setLinearVelocity(
				  OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10.0f ); // shooting speed
			   // push the created objects to the deques
			mShapes.push_back(sceneCylinderShape);
			mBodies.push_back(defaultBody);
			mToggle = 0.5;
		}
		// create and throw a sphere if 'S' is pressed
		if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_S) && mToggle <=0)
		{
			Vector3 size = Vector3::ZERO;   // size of the box
			// starting position of the box
			Vector3 position = (OgreFramework::getSingletonPtr()->m_pCamera->getDerivedPosition() +
							OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10);
			// create an ordinary, Ogre mesh with texture
			Entity *entity = m_pSceneMgr->createEntity(
			   "sphere" + StringConverter::toString(mNumEntitiesInstanced),
			   "sphere.mesh");
			entity->setCastShadows(true);
			// we need the bounding box of the box to be able to set the size of the Bullet-box
			AxisAlignedBox boundingB = entity->getBoundingBox();
			size = boundingB.getSize(); size /= 2.0f; // only the half needed
//			size *= 0.96f;   // Bullet margin is a bit bigger so we need a smaller size
						   // (Bullet 2.76 Physics SDK Manual page 18)
			entity->setMaterialName("Examples/BumpyMetal");
			SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
			node->attachObject(entity);
			node->scale(0.01f, 0.01f, 0.01f);   // the cube is too big for us
			size *= 0.01f;                  // don't forget to scale down the Bullet-box too
			// after that create the Bullet shape with the calculated size
			OgreBulletCollisions::SphereCollisionShape *sceneSphereShape = new OgreBulletCollisions::SphereCollisionShape(1);
			// and the Bullet rigid body
			OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
			   "defaultBoxRigid" + StringConverter::toString(mNumEntitiesInstanced),
			   m_BulletWorld);


			defaultBody->setShape(   node,
						   sceneSphereShape,
						   0.9f,         // dynamic body restitution
						   0.7f,         // dynamic body friction
						   1.0f,          // dynamic bodymass
						   position,      // starting position of the box
						   Quaternion(0, 0, 0, 1));// orientation of the box
			mNumEntitiesInstanced++;
			defaultBody->setLinearVelocity(
				  OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10.0f ); // shooting speed
			   // push the created objects to the deques
			mShapes.push_back(sceneSphereShape);
			mBodies.push_back(defaultBody);
			mToggle = 0.5;
		}
		// objet
		if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_O) && mToggle <=0)
		{
			Vector3 size = Vector3::ZERO;   // size of the box
			// starting position of the box
			Vector3 position = (OgreFramework::getSingletonPtr()->m_pCamera->getDerivedPosition() +
							OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 10);
			// create an ordinary, Ogre mesh with texture
			Entity *entity = m_pSceneMgr->createEntity(
			   "sphere" + StringConverter::toString(mNumEntitiesInstanced),
			   "penguin.mesh");
			SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
			node->attachObject(entity);
			float scale = 0.1f;
			node->scale(scale, scale, scale);   // the cube is too big for us

			// we need the bounding box of the box to be able to set the size of the Bullet-box
			AxisAlignedBox boundingB = entity->getBoundingBox();
			size = boundingB.getSize(); size /= 2.0f; // only the half needed
//			size *= 0.96f;   // Bullet margin is a bit bigger so we need a smaller size
						   // (Bullet 2.76 Physics SDK Manual page 18)
			size *= scale;                  // don't forget to scale down the Bullet-box too
			// after that create the Bullet shape with the calculated size

			Ogre::Matrix4 mat_scale = Ogre::Matrix4::IDENTITY;
			mat_scale.setScale(Ogre::Vector3(scale));

			OgreBulletCollisions::AnimatedMeshToShapeConverter *converter = new OgreBulletCollisions::AnimatedMeshToShapeConverter(entity, mat_scale);
//			OgreBulletCollisions::TriangleMeshCollisionShape* shape = converter->createTrimesh();
			OgreBulletCollisions::CollisionShape* shape = (OgreBulletCollisions::CollisionShape*)converter->createConvexDecomposition();


			// and the Bullet rigid body
			OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
			   "defaultBoxRigid" + StringConverter::toString(mNumEntitiesInstanced),
			   m_BulletWorld);


			defaultBody->setShape(   node,
						   shape,
						   0.5f,         // dynamic body restitution
						   0.5f,         // dynamic body friction
						   1.0f,          // dynamic bodymass
						   position,      // starting position of the box
						   Quaternion(1, 0, 0, 1.57));// orientation of the box
			mNumEntitiesInstanced++;
			defaultBody->setLinearVelocity(
				  OgreFramework::getSingletonPtr()->m_pCamera->getDerivedDirection().normalisedCopy() * 30.0f ); // shooting speed
			   // push the created objects to the deques
			mShapes.push_back(shape);
			mBodies.push_back(defaultBody);
			mToggle = 0.5;
		}
	}

	if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F))
	{
			//do something
	}


	if (mVolumeRoot)
	{
		if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F10))
		{
			mVolumeRoot->setVolumeVisible(!mVolumeRoot->getVolumeVisible());
		}
		if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F11))
		{
			mVolumeRoot->setOctreeVisible(!mVolumeRoot->getOctreeVisible());
		}
		if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F12))
		{
			mVolumeRoot->setDualGridVisible(!mVolumeRoot->getDualGridVisible());
		}
	}


	return true;
}





//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	return OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (id == OIS::MB_Left)
	{
		Ray ray = OgreFramework::getSingletonPtr()->m_pCamera->getCameraToViewportRay(0.5, 0.5);
		shootRay(ray, false);
	}

	return OgreFramework::getSingletonPtr()->mousePressed(evt, id);
}

bool DemoApp::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (id == OIS::MB_Middle || id == OIS::MB_Left)
	{
		mMouseState = 0;
	}

	return OgreFramework::getSingletonPtr()->mouseReleased(evt, id);
}


bool DemoApp::mouseMoved(const OIS::MouseEvent &evt)
{
	mMouseX = (Real)evt.state.X.abs / (Real)evt.state.width;
	mMouseY = (Real)evt.state.Y.abs / (Real)evt.state.height;

	return OgreFramework::getSingletonPtr()->mouseMoved(evt);
}



//-------------------------------------------------------------------------------------
void DemoApp::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if (mTerrainGroup)
	{
		if (mTerrainGroup->isDerivedDataUpdateInProgress())
		{
			OgreFramework::getSingletonPtr()->m_pTrayMgr->moveWidgetToTray(mInfoLabel, OgreBites::TL_TOP, 0);
			mInfoLabel->show();
			if (mTerrainsImported)
			{
				mInfoLabel->setCaption("Building terrain, please wait...");
			}
			else
			{
				mInfoLabel->setCaption("Updating textures, patience...");
			}
		}
		else
		{
			OgreFramework::getSingletonPtr()->m_pTrayMgr->removeWidgetFromTray(mInfoLabel);
			mInfoLabel->hide();
			if (mTerrainsImported)
			{
				mTerrainGroup->saveAllTerrains(true);
				mTerrainsImported = false;
			}
		}

		// controle de l'altitude de la caméra = 1.8 m de la surface du sol
		Ogre::Vector3 pos = OgreFramework::getSingletonPtr()->m_pCamera->getPosition();
		pos.y = 1.8 + mTerrainGroup->getHeightAtWorldPosition(pos);
		OgreFramework::getSingletonPtr()->m_pCamera->setPosition(pos);
	}

	// update bullet
	if (m_BulletWorld)
		m_BulletWorld->stepSimulation(evt.timeSinceLastFrame);   // update Bullet Physics animation


	// la course du soleil
//	if(mSpinLight)
	{
		mLightPivot->rotate(mLightRotationAxe, Ogre::Angle(evt.timeSinceLastFrame * -0.0005f));
	}

}














////////////////////////////// HEIGHTMAP TERRAIN


void DemoApp::createTerrain(Ogre::Light* light)
{
	// terrain
	// How large is a page of tiles (in vertices)? Must be (2^n)+1
	m_TerrainPageSize = 513;
	// The size of a terrain page, in world units
	m_TerrainPageWorld=12000.0f;
	// Maximum height of the terrain
	m_TerrainMaxHeight=600.f;

	mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();

	mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(
												m_pSceneMgr,
												Ogre::Terrain::ALIGN_X_Z,
												m_TerrainPageSize,
												m_TerrainPageWorld
												);
	mTerrainGroup->setFilenameConvention(Ogre::String("TutorialTerrain"), Ogre::String("dat"));
	mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);
	configureTerrainDefaults(light);

	for (long x = 0; x <= 0; ++x)
		for (long y = 0; y <= 0; ++y)
			defineTerrain(x, y);

	// sync load since we want everything in place when we start
	mTerrainGroup->loadAllTerrains(true);

	if (mTerrainsImported)
	{
		Ogre::TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
		while(ti.hasMoreElements())
		{
			Ogre::Terrain* t = ti.getNext()->instance;
			initBlendMaps(t);
		}
	}
	mTerrainGroup->freeTemporaryResources();



	// ground
	const int		nb_adventices = 8;
	Ogre::String	adventices[nb_adventices];
	Ogre::Entity	*E_adventice[nb_adventices];
	adventices[0] = "Paturin";
	adventices[1] = "Pissenlit";
	adventices[2] = "Crocus";
	adventices[3] = "Cigue";
	adventices[4] = "Muguet";
	adventices[5] = "Lavande";
	adventices[6] = "Mauve";
	adventices[7] = "Moutarde";
	for (int i=0; i<nb_adventices; i++)
	{
		createGrassMesh(adventices[i]);
		E_adventice[i] = m_pSceneMgr->createEntity("e"+adventices[i], adventices[i]);
	}

//	Ogre::InstanceManager *IM = m_pSceneMgr->createInstanceManager("IM", "mesh_name", "group_name", Ogre::InstanceManager::ShaderBased, 1 );
	//	Ogre::InstanceManager::InstancingTechnique::HWInstancingBasic

	// tree
//	Ogre::SceneNode* headNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("HeadNode");
	Ogre::Entity* maison = m_pSceneMgr->createEntity("maison", "tudorhouse.mesh");
	maison->setCastShadows(true);
	Ogre::Entity* tree = m_pSceneMgr->createEntity("arbre", "column.mesh");
//	Ogre::Entity* tree = m_pSceneMgr->createEntity("arbre", "Tree02A.mesh");
	tree->setCastShadows(true);


	tree->setMaterialName("myMaterials/Bark");

//	Ogre::InstancedGeometry *sg = m_pSceneMgr->createInstancedGeometry("GroundArea");
	Ogre::StaticGeometry *sg = m_pSceneMgr->createStaticGeometry("GroundArea");

	const int size = 1000;
	const int rarete = 30;
	sg->setRegionDimensions(Ogre::Vector3(size, size, size));
	sg->setOrigin(Ogre::Vector3(-size/2, 0, -size/2));
	for (int x = -size/2; x < size/2; x += rarete)
	{
		for (int z = -size/2; z < size/2; z += rarete)
		{
			Ogre::Real x1 = x + Ogre::Math::RangeRandom(-rarete/2, rarete/2);
			Ogre::Real z1 = z + Ogre::Math::RangeRandom(-rarete/2, rarete/2);

			// positionner au niveau du terrain
			Ogre::Vector3 pos(x1, 0, z1);
			long slotx;
			long sloty;
			mTerrainGroup->convertWorldPositionToTerrainSlot(pos, &slotx, &sloty);
			Ogre::Terrain *terrain = mTerrainGroup->getTerrain(slotx, sloty);
			pos.y = terrain->getHeightAtWorldPosition(pos);



			Ogre::Vector3 scale(0.2f, Ogre::Math::RangeRandom(0.18f, 0.22f), 0.2f);
			Ogre::Quaternion orientation;
			orientation.FromAngleAxis(Ogre::Degree(Ogre::Math::RangeRandom(0, 359)), Ogre::Vector3::UNIT_Y);

			float rand = Ogre::Math::RangeRandom(0.f, 100.f);
			if (rand < 30)
				sg->addEntity(E_adventice[0], pos, orientation, scale);
			else if (rand < 60)
			{
				orientation.FromAngleAxis(Ogre::Degree(0), Ogre::Vector3::UNIT_X);
				sg->addEntity(tree, pos, orientation, Ogre::Vector3(Ogre::Math::RangeRandom(0.01f, 0.1f)));
//				orientation.FromAngleAxis(Ogre::Degree(-90), Ogre::Vector3::UNIT_X);
//				sg->addEntity(tree, pos, orientation, Ogre::Vector3(Ogre::Math::RangeRandom(0.1f, 1.f)));
			}
			else if (rand < 65)
			{
				pos += Ogre::Vector3(0, 10.f, 0);
				scale = Ogre::Vector3(0.02f);

				sg->addEntity(maison, pos, orientation, scale);
//				add_Entity_to_Bullet(maison, pos, orientation, scale);
			}
			else if (rand < 70)
				sg->addEntity(E_adventice[1], pos, orientation, scale);
			else if (rand < 75)
				sg->addEntity(E_adventice[2], pos, orientation, scale);
			else if (rand < 80)
				sg->addEntity(E_adventice[3], pos, orientation, scale);
			else if (rand < 85)
				sg->addEntity(E_adventice[4], pos, orientation, scale);
			else if (rand < 90)
				sg->addEntity(E_adventice[5], pos, orientation, scale);
			else if (rand < 95)
				sg->addEntity(E_adventice[6], pos, orientation, scale);
			else
				sg->addEntity(E_adventice[7], pos, orientation, scale);
		}
	}
	sg->setCastShadows(true);
	sg->build();
}

//-------------------------------------------------------------------------------------
void DemoApp::configureTerrainDefaults(Ogre::Light* light)
{
	// Configure global
	mTerrainGlobals->setMaxPixelError(8);
	// testing composite map
	mTerrainGlobals->setCompositeMapDistance(3000);

	// Important to set these so that the terrain knows what to use for derived (non-realtime) data
	mTerrainGlobals->setLightMapDirection(light->getDerivedDirection());
	mTerrainGlobals->setCompositeMapAmbient(m_pSceneMgr->getAmbientLight());
	mTerrainGlobals->setCompositeMapDiffuse(light->getDiffuseColour());

	// Configure default import settings for if we use imported image
	Ogre::Terrain::ImportData& defaultimp = mTerrainGroup->getDefaultImportSettings();
	defaultimp.terrainSize = 4000;
	defaultimp.worldSize = m_TerrainPageWorld;
	defaultimp.inputScale = m_TerrainMaxHeight; // due terrain.png is 8 bpp
	defaultimp.minBatchSize = 33;
	defaultimp.maxBatchSize = 65;

	// textures
	defaultimp.layerList.resize(3);
	defaultimp.layerList[0].worldSize = 100;
	defaultimp.layerList[0].textureNames.push_back("grass_green-01_diffusespecular.dds");
	defaultimp.layerList[0].textureNames.push_back("grass_green-01_normalheight.dds");
	defaultimp.layerList[1].worldSize = 30;
	defaultimp.layerList[1].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");
	defaultimp.layerList[1].textureNames.push_back("dirt_grayrocky_normalheight.dds");
	defaultimp.layerList[2].worldSize = 200;
	defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");
	defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");

}




//-------------------------------------------------------------------------------------
void DemoApp::defineTerrain(long x, long y)
{
	Ogre::String filename = mTerrainGroup->generateFilename(x, y);
	if (Ogre::ResourceGroupManager::getSingleton().resourceExists(mTerrainGroup->getResourceGroup(), filename))
	{
		mTerrainGroup->defineTerrain(x, y);
	}
	else
	{
		Ogre::Image img;
		getTerrainImage(x % 2 != 0, y % 2 != 0, img);
		mTerrainGroup->defineTerrain(x, y, &img);
		mTerrainsImported = true;
	}
}


//-------------------------------------------------------------------------------------
void DemoApp::getTerrainImage(bool flipX, bool flipY, Ogre::Image& img)
{
	img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	if (flipX)
		img.flipAroundY();
	if (flipY)
		img.flipAroundX();

}
//-------------------------------------------------------------------------------------
void DemoApp::initBlendMaps(Ogre::Terrain* terrain)
{

}


//-------------------------------------------------------------------------------------
void DemoApp::createGrassMesh(Ogre::String str)
{
	const float width = 5.f;
	const float height = 6.f;
	Ogre::ManualObject mo(str);

	Ogre::Vector3 vec(width/2, 0, 0);
	Ogre::Quaternion rot;
	rot.FromAngleAxis(Ogre::Degree(90), Ogre::Vector3::UNIT_Y);

	mo.begin("myMaterials/"+str, Ogre::RenderOperation::OT_TRIANGLE_LIST);
	for (int i = 0; i < 2; ++i)
	{
		mo.position(-vec.x, height, -vec.z);
		mo.textureCoord(0, 0);

		mo.position(vec.x, height, vec.z);
		mo.textureCoord(1, 0);

		mo.position(-vec.x, 0, -vec.z);
		mo.textureCoord(0, 1);

		mo.position(vec.x, 0, vec.z);
		mo.textureCoord(1, 1);

		int offset = i * 4;
		mo.triangle(offset, offset+3, offset+1);
		mo.triangle(offset, offset+2, offset+3);

		vec = rot * vec;
	}
	mo.end();
	mo.convertToMesh(str);
}
















//////////////////////////////////////////////////////// BULLET

void DemoApp::initBullet()
{
	// Start Bullet
	m_BulletWorld = new OgreBulletDynamics::DynamicsWorld(
		m_pSceneMgr,
		Ogre::AxisAlignedBox (
			Ogre::Vector3 (-10000, -10000, -10000), //aligned box for Bullet
			Ogre::Vector3 (10000,  10000,  10000)
			),
		Ogre::Vector3(0,-9.81f,0)
		);
	// add Debug info display tool
/*	debugDrawer = new OgreBulletCollisions::DebugDrawer();
	debugDrawer->setDrawWireframe(true);   // we want to see the Bullet containers
	m_BulletWorld->setDebugDrawer(debugDrawer);
	m_BulletWorld->setShowDebugShapes(true);      // enable it if you want to see the Bullet containers
	Ogre::SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("debugDrawer", Ogre::Vector3::ZERO);
	node->attachObject(static_cast <SimpleRenderable *> (debugDrawer));
*/

	setTerrainPhysics();
}

void DemoApp::setTerrainPhysics()
{
	// load the heightmap image
	Ogre::Image img;
	img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);


	// compute the height array
	unsigned page_size = mTerrainGroup->getTerrainSize();

m_pLog->logMessage("-> DemoApp::setTerrainPhysics page_size=" + StringConverter::toString(page_size));

	Ogre::Vector3 terrainScale(m_TerrainPageWorld / (page_size-1), m_TerrainMaxHeight, m_TerrainPageWorld / (page_size-1));

	float *heights = new float[page_size*page_size];
	for(unsigned y = 0; y < page_size; ++y)
	{
		for(unsigned x = 0; x < page_size; ++x)
		{
			Ogre::ColourValue color = img.getColourAt(x, y, 0);
			heights[x + y * page_size] = color.r;
		}
	}

	// create the bullet shape and rigid body
	OgreBulletCollisions::HeightmapCollisionShape *terrainShape = new OgreBulletCollisions::HeightmapCollisionShape(
		page_size,
		page_size,
		terrainScale,
		heights,
		true
	);
	OgreBulletDynamics::RigidBody *terrainBody = new OgreBulletDynamics::RigidBody(
		"Terrain",
		m_BulletWorld
	);


	Ogre::Vector3 terrainShiftPos(terrainScale.x/(page_size-1), 0, terrainScale.z/(page_size-1));
	terrainShiftPos.y = terrainScale.y / 2 * terrainScale.y;

	Ogre::SceneNode *pTerrainNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	terrainBody->setStaticShape(pTerrainNode, terrainShape, 0.0f, 0.8f, terrainShiftPos);
	terrainBody->setPosition(terrainBody->getWorldPosition());

	// push the created objects to the deques
	mShapes.push_back(terrainShape);
	mBodies.push_back(terrainBody);
}



void DemoApp::add_Entity_to_Bullet(Entity *entity, const Vector3 &position, const Quaternion &quat, const Vector3 &scale)
{
	m_pLog->logMessage("-> add_Entity_to_Bullet");
	// we need the bounding box of the box to be able to set the size of the Bullet-box
	AxisAlignedBox boundingB = entity->getBoundingBox();
	Vector3 size = boundingB.getSize(); size /= 2.0f; // only the half needed
//	size *= 0.96f;   // Bullet margin is a bit bigger so we need a smaller size
				   // (Bullet 2.76 Physics SDK Manual page 18)

	m_pLog->logMessage("		sceneBoxShape");
	// after that create the Bullet shape with the calculated size
	OgreBulletCollisions::StaticMeshToShapeConverter *converter = new OgreBulletCollisions::StaticMeshToShapeConverter(entity);
	OgreBulletCollisions::CollisionShape* shape = (OgreBulletCollisions::CollisionShape*)converter->createConvexDecomposition();

	// and the Bullet rigid body
	OgreBulletDynamics::RigidBody *defaultBody = new OgreBulletDynamics::RigidBody(
	   "defaultBoxRigid_" + entity->getName(),
	   m_BulletWorld);
	SceneNode *node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(entity);
	node->scale(scale);

	m_pLog->logMessage("		setShape");
	m_pLog->logMessage("		defaultBody="+StringConverter::toString(defaultBody));
	m_pLog->logMessage("		node="+StringConverter::toString(node));
	m_pLog->logMessage("		eBoxShape="+StringConverter::toString(shape));
	m_pLog->logMessage("		position="+StringConverter::toString(position));
	m_pLog->logMessage("		quat="+StringConverter::toString(quat));

	defaultBody->setShape(node,
				   shape,
				   0.7f,         // dynamic body restitution
				   0.6f,         // dynamic body friction
				   1.0f,          // dynamic bodymass
				   position,      // starting position of the box
				   quat);// orientation of the box
	mNumEntitiesInstanced++;

	m_pLog->logMessage("		push_back");
	// push the created objects to the deques
	mShapes.push_back(shape);
	mBodies.push_back(defaultBody);
}


























//////////////////////////////////////////// VOLUME TERRAIN

void DemoApp::initVolumeTerrain()
{
m_pLog->logMessage("-> DemoApp::initVolumeTerrain");
	// Volume
	mVolumeRoot = OGRE_NEW Chunk();
	mVolumeRootNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("VolumeParent");
	m_pLog->logMessage("BEGIN Load volume terrain");

	myMeshBuilderCallback callback(this);
	mVolumeRoot->load(mVolumeRootNode, m_pSceneMgr, "volumeTerrain.cfg", false, &callback);
//	mVolumeRoot->load(mVolumeRootNode, m_pSceneMgr, "volumeTerrain.cfg", true);

//	mVolumeRoot->getChunkParameters()->createOctreeVisualization = true;
//	mVolumeRoot->getChunkParameters()->createDualGridVisualization = true;

	m_pLog->logMessage("END Loaded volume terrain");


	// Camera
	OgreFramework::getSingletonPtr()->m_pCamera->setPosition((Real)2800, (Real)2200, (Real)3000);
	OgreFramework::getSingletonPtr()->m_pCamera->lookAt((Real)0, (Real)100, (Real)0);
	OgreFramework::getSingletonPtr()->m_pCamera->setNearClipDistance((Real)0.5);

m_pLog->logMessage("<- DemoApp::initVolumeTerrain");
}



void DemoApp::setVolumeTerrainPhysics()
{
/*
	// create the bullet shape and rigid body
	OgreBulletCollisions::StaticMeshToShapeConverter *terrainShape = new OgreBulletCollisions::StaticMeshToShapeConverter(
		page_size,
		page_size,
		terrainScale,
		heights,
		true
	);
	OgreBulletDynamics::RigidBody *terrainBody = new OgreBulletDynamics::RigidBody(
		"Terrain",
		m_BulletWorld
	);


	Ogre::Vector3 terrainShiftPos(terrainScale.x/(page_size-1), 0, terrainScale.z/(page_size-1));
	terrainShiftPos.y = terrainScale.y / 2 * terrainScale.y;

	Ogre::SceneNode *pTerrainNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	terrainBody->setStaticShape(pTerrainNode, terrainShape, 0.0f, 0.8f, terrainShiftPos);
	terrainBody->setPosition(terrainBody->getWorldPosition());

	// push the created objects to the deques
	mShapes.push_back(terrainShape);
	mBodies.push_back(terrainBody);
*/
}




void DemoApp::shootRay(Ray ray, bool doUnion)
{
	Vector3 intersection;
	Real scale = mVolumeRoot->getChunkParameters()->scale;
	bool intersects = mVolumeRoot->getChunkParameters()->src->getFirstRayIntersection(ray, intersection, scale);

	if (intersects and OgreFramework::getSingletonPtr()->m_pCamera->getPosition().distance(intersection * scale) < 30)
	{

		Real radius = (Real)2.5;
		CSGSphereSource sphere(radius, intersection);
		CSGOperationSource *operation = doUnion ? static_cast<CSGOperationSource*>(new CSGUnionSource()) : new CSGDifferenceSource();
		static_cast<TextureSource*>(mVolumeRoot->getChunkParameters()->src)->combineWithSource(operation, &sphere, intersection, radius * (Real)1.5);

		mVolumeRoot->getChunkParameters()->updateFrom = intersection - radius * (Real)1.5;
		mVolumeRoot->getChunkParameters()->updateTo = intersection + radius * (Real)1.5;
		mVolumeRoot->load(mVolumeRootNode, Vector3::ZERO, Vector3(384), 5, mVolumeRoot->getChunkParameters());
		delete operation;
	}
}




