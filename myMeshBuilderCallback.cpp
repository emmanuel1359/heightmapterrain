
#include "myMeshBuilderCallback.h"

#include "OgreFramework.h"
#include <OgreVolumeMeshBuilder.h>

#include "OgreBulletDynamics.h"
#include "OgreBulletCollisions.h"
#include "OgreBulletCollisionsShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"


using namespace Ogre;
using namespace OgreBulletCollisions;


myMeshBuilderCallback::myMeshBuilderCallback(DemoApp* app)
{
	m_ptheApp = app;
}

void myMeshBuilderCallback::ready(const SimpleRenderable *simpleRenderable, const VecVertex &vertices, const VecIndices &indices, size_t level, int inProcess)
{
OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready");

	unsigned int vertexCount = vertices.size();
	unsigned int indexCount = indices.size();
	Ogre::Vector3* pVertices = new Ogre::Vector3[vertexCount];
	unsigned int *pIndices = new unsigned int[indexCount];
OgreFramework::getSingletonPtr()->m_pLog->logMessage("vertexCount = "+StringConverter::toString(vertexCount));
OgreFramework::getSingletonPtr()->m_pLog->logMessage("indexCount = "+StringConverter::toString(indexCount));
OgreFramework::getSingletonPtr()->m_pLog->logMessage("level = "+StringConverter::toString(level));
OgreFramework::getSingletonPtr()->m_pLog->logMessage("inProcess = "+StringConverter::toString(inProcess));

	if (vertexCount > 0)
	{
		for (VecVertex::const_iterator it = vertices.begin(); it != vertices.end(); it++)
		{
			pVertices->x = it->x;
			pVertices->y = it->y;
			pVertices->z = it->z;
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("pVertices->x = "+StringConverter::toString(pVertices->x));
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("pVertices->y = "+StringConverter::toString(pVertices->y));
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("pVertices->z = "+StringConverter::toString(pVertices->z));
			pVertices++;
		}
		for (VecIndices::const_iterator it = indices.begin(); it != indices.end(); it++)
		{
			*pIndices = *it;
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("*pIndices = "+StringConverter::toString(*pIndices));
			pIndices++;
		}

		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready terrainShape");


			// create the bullet shape and rigid body
//		OgreBulletCollisions::StaticMeshToShapeConverter *terrainShape = new OgreBulletCollisions::StaticMeshToShapeConverter(const_cast<SimpleRenderable*> (simpleRenderable), Ogre::Matrix4::IDENTITY);

		OgreBulletCollisions::TriangleMeshCollisionShape *terrainShape = new OgreBulletCollisions::TriangleMeshCollisionShape(pVertices, vertexCount, pIndices, indexCount, false);

		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready delete");
		delete pVertices;
		delete pIndices;

		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready terrainBody");

		OgreBulletDynamics::RigidBody *terrainBody = new OgreBulletDynamics::RigidBody(
			"VolumeTerrain",
			m_ptheApp->getBulletWorld()
		);


	//		Ogre::Vector3 terrainShiftPos(terrainScale.x/(page_size-1), 0, terrainScale.z/(page_size-1));
	//		terrainShiftPos.y = terrainScale.y / 2 * terrainScale.y;
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready pTerrainNode");

		Ogre::SceneNode *pTerrainNode = OgreFramework::getSingletonPtr()->m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
		const Ogre::Vector3 v3 = Ogre::Vector3::ZERO;
		const Ogre::Quaternion q = Ogre::Quaternion::IDENTITY;

		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready setStaticShape");
//		terrainBody->setStaticShape(terrainShape, 0.0f, 0.8f, v3, q);
		terrainBody->setStaticShape(pTerrainNode, terrainShape, 0.0f, 0.8f, v3, q);
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready setPosition");
		terrainBody->setPosition(terrainBody->getWorldPosition());

		OgreFramework::getSingletonPtr()->m_pLog->logMessage("-> myMeshBuilderCallback::ready push_back");
		// push the created objects to the deques
		m_ptheApp->getBulletShapes().push_back(terrainShape);
		m_ptheApp->getBulletBodies().push_back(terrainBody);
	}
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("<- myMeshBuilderCallback::ready");
}
