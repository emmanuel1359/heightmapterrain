//|||||||||||||||||||||||||||||||||||||||||||||||

#ifndef OGRE_DEMO_HPP
#define OGRE_DEMO_HPP

//|||||||||||||||||||||||||||||||||||||||||||||||

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include "OgreFramework.h"
#include "OgreVolumeChunk.h"

#include "OgreBulletDynamics.h"
#include "OgreBulletCollisions.h"
#include "OgreBulletCollisionsShape.h"
#include "Shapes/OgreBulletCollisionsBoxShape.h"       // for Boxes
#include "Shapes/OgreBulletCollisionsStaticPlaneShape.h" // for static planes
#include "Shapes/OgreBulletCollisionsCylinderShape.h"       // for cylinders
#include "Shapes/OgreBulletCollisionsSphereShape.h"       // for spheres
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Shapes/OgreBulletCollisionsTerrainShape.h"

using namespace Ogre;
using namespace Ogre::Volume;
using namespace OgreBulletDynamics;


//|||||||||||||||||||||||||||||||||||||||||||||||

class DemoApp : public OIS::KeyListener, OIS::MouseListener
{
public:
	DemoApp();
	~DemoApp();

	void startDemo();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseMoved(const OIS::MouseEvent &evt);

	OgreBulletDynamics::DynamicsWorld* getBulletWorld() {return m_BulletWorld;}
	std::deque<OgreBulletDynamics::RigidBody *> getBulletBodies() {return mBodies;}
	std::deque<OgreBulletCollisions::CollisionShape *> getBulletShapes() {return mShapes;}

private:
	void setupDemoScene();
	void runDemo();

	bool					m_bShutdown;
	double					m_timeSinceLastFrame;
	Ogre::SceneManager*		m_pSceneMgr;
	Ogre::Log*				m_pLog;

	Ogre::TerrainGlobalOptions* mTerrainGlobals;
	Ogre::TerrainGroup* mTerrainGroup;
	bool mTerrainsImported;

	OgreBites::Label* mInfoLabel;

	// How large is a page of tiles (in vertices)? Must be (2^n)+1
	int						m_TerrainPageSize;
	// The size of a terrain page, in world units
	Ogre::Real				m_TerrainPageWorld;
	// Maximum height of the terrain
	Ogre::Real				m_TerrainMaxHeight;

	void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img);
	void defineTerrain(long x, long y);
	void initBlendMaps(Ogre::Terrain* terrain);
	void configureTerrainDefaults(Ogre::Light* light);
	virtual void createGrassMesh(Ogre::String);
	void createTerrain(Ogre::Light* light);

	// BULLET
	OgreBulletDynamics::DynamicsWorld *m_BulletWorld;   // OgreBullet World
	std::deque<OgreBulletDynamics::RigidBody *>         mBodies;
	std::deque<OgreBulletCollisions::CollisionShape *>  mShapes;
	void initBullet();
	OgreBulletCollisions::DebugDrawer *debugDrawer;
	int mNumEntitiesInstanced;
    void setTerrainPhysics();
	void add_Entity_to_Bullet(Entity *entity, const Vector3 &position, const Quaternion &quat, const Vector3 &scale);

	void frameRenderingQueued(const Ogre::FrameEvent& evt);

//	virtual void createFrameListener(void);
//	virtual void destroyScene(void);
//	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	// volume terrain
	void initVolumeTerrain();
    void shootRay(Ray ray, bool doUnion);
    void setVolumeTerrainPhysics();

    /// Holds the volume root.
    Chunk *mVolumeRoot;

    /// The node on which the terrain is attached.
    SceneNode *mVolumeRootNode;

    /// To show or hide everything.
    bool mHideAll;

    /// Whether we bevel, emboss or do nothing with the mouse.
    int mMouseState;

    /// A countdown when the next mouse modifier update will happen.
    Real mMouseCountdown;

    /// Current mouse position, X-part.
    Real mMouseX;

    /// Current mouse position, Y-part.
    Real mMouseY;


    // soleil
	Ogre::Light* mLight;
	// billboards for lights
	Ogre::BillboardSet* mLightFlareSet;
	Ogre::Billboard* mLightFlare;
	// Positions for lights
	Ogre::Vector3 mLightPosition;
	Ogre::Vector3 mLightRotationAxe;
	// Which lights are enabled
	bool mLightState;
	// the light nodes
	Ogre::SceneNode* mLightNode;
	// the light node pivots
	Ogre::SceneNode* mLightPivot;

};

//|||||||||||||||||||||||||||||||||||||||||||||||

#endif

//|||||||||||||||||||||||||||||||||||||||||||||||
