//|||||||||||||||||||||||||||||||||||||||||||||||

#ifndef myMeshBuilderCallback_h
#define myMeshBuilderCallback_h

//|||||||||||||||||||||||||||||||||||||||||||||||

#include "OgreVolumeMeshBuilder.h"
#include "OgreVolumeChunk.h"
#include "DemoApp.h"


using namespace Ogre;
using namespace Ogre::Volume;


//|||||||||||||||||||||||||||||||||||||||||||||||

class myMeshBuilderCallback : public MeshBuilderCallback
{
public:
	myMeshBuilderCallback(DemoApp* app);

	void ready(const SimpleRenderable *simpleRenderable, const VecVertex &vertices, const VecIndices &indices, size_t level, int inProcess);

	DemoApp* m_ptheApp;
};


//|||||||||||||||||||||||||||||||||||||||||||||||

#endif

//|||||||||||||||||||||||||||||||||||||||||||||||
